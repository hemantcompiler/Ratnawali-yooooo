package com.ratnawali.ratnawali_2014;


import com.ratnawali.ratnawali_2014.R;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
 
public class ContactUs extends Fragment {
 
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
    	View rootView = inflater.inflate(R.layout.webview, container, false);
    	WebView wv = (WebView)rootView.findViewById(R.id.webview);
        wv.loadUrl("file:///android_asset/contact.html");
        return rootView;
    }
}