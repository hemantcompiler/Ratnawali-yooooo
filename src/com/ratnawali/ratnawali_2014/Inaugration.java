package com.ratnawali.ratnawali_2014;


import com.ratnawali.ratnawali_2014.R;


import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

public class Inaugration extends FragmentActivity {

	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		setContentView(R.layout.webview);
		final WebView wv = (WebView)findViewById(R.id.webview);
		
		wv.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedError(WebView view, int errorCode,
                    String description, String failingUrl) {
            	wv.loadUrl("file:///android_asset/Inaugration.html");
               
            }
            
            
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
		
		Button newButton = (Button)findViewById(R.id.new_button);
		newButton.setOnClickListener(new View.OnClickListener() {
 		   public void onClick(View v) {
 			   wv.loadUrl("http://ratnawali.netii.net/Inaugration.php");
 		   }});
 	
		wv.getSettings().setJavaScriptEnabled(true);
        wv.loadUrl("http://ratnawali.netii.net/Inaugration.php");
		
		
	}

}