package com.ratnawali.ratnawali_2014;


import com.ratnawali.ratnawali_2014.R;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
 
public class HistoryFragment extends Fragment { 
	 
    @SuppressLint({ "SetJavaScriptEnabled", "NewApi" }) @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
    	
    	View rootView = inflater.inflate(R.layout.webview, container, false);
    	final WebView wv = (WebView)rootView.findViewById(R.id.webview);
    	
        //wv.loadUrl("file:///android_asset/sponsers.html");
    	
    	Button newButton = (Button)rootView.findViewById(R.id.new_button);
    	
    	newButton.setOnClickListener(new View.OnClickListener() {
    		   public void onClick(View v) {
    			   wv.loadUrl("http://ratnawali.netii.net/history.php");
    		   }});
    	
    	wv.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedError(WebView view, int errorCode,
                    String description, String failingUrl) {
            	wv.loadUrl("file:///android_asset/history.html");
               
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
    	
   
    	
		wv.getSettings().setJavaScriptEnabled(true);
		wv.getSettings().setLoadWithOverviewMode(true);
   	 	wv.getSettings().setUseWideViewPort(true);
   	 	wv.getSettings().setBuiltInZoomControls(true);
   	 	wv.getSettings().setDisplayZoomControls(false);
   	 	
		wv.loadUrl("http://ratnawali.netii.net/history.php");
    	

        return rootView; 
    }
}