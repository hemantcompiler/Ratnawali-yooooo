package com.ratnawali.ratnawali_2014;


import com.ratnawali.ratnawali_2014.R;
import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBar.TabListener;

@SuppressLint("NewApi") public class MainActivity extends FragmentActivity implements TabListener, android.app.ActionBar.TabListener {

	 private ViewPager viewPager;
	    private TaskPagerAdapter mAdapter;
	    private android.app.ActionBar actionBar;
	    // Tab titles
	    private String[] tabs = { "Live", "About", "Events","Gallery","Contact Us"};
	    
	    
	 
	    protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        SharedPreferences myPrefs = getSharedPreferences("myPrefs", MODE_PRIVATE);
	        setContentView(R.layout.activity_main);
	 
	        // Initilization
	        viewPager = (ViewPager) findViewById(R.id.pager);
	        actionBar = getActionBar();
	        mAdapter = new TaskPagerAdapter(getSupportFragmentManager());
	 
	        viewPager.setAdapter(mAdapter);
	        actionBar.setHomeButtonEnabled(false);
	        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);       
	 
	        // Adding Tabs
	        for (String tab_name : tabs) {
	            actionBar.addTab(actionBar.newTab().setText(tab_name).setTabListener(this));
	        }

	    viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
	    	 
	        @Override
	        public void onPageSelected(int position) {
	            // on changing the page
	            // make respected tab selected
	            actionBar.setSelectedNavigationItem(position);
	        }
	     
	        @Override
	        public void onPageScrolled(int arg0, float arg1, int arg2) {
	        }
	     
	        @Override
	        public void onPageScrollStateChanged(int arg0) {
	        }
	    });
	    }


		@Override
		public void onTabReselected(Tab arg0, FragmentTransaction arg1) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onTabSelected(Tab arg0, FragmentTransaction arg1) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onTabUnselected(Tab arg0, FragmentTransaction arg1) {
			// TODO Auto-generated method stub
			
		}


		@Override
		public void onTabReselected(android.app.ActionBar.Tab tab,
				android.app.FragmentTransaction ft) {
			// TODO Auto-generated method stub
			
		}


		@Override
		public void onTabSelected(android.app.ActionBar.Tab tab,
				android.app.FragmentTransaction ft) {
			// TODO Auto-generated method stub
			// on tab selected
	        // show respected fragment view
	        viewPager.setCurrentItem(tab.getPosition());
			
		}


		@Override
		public void onTabUnselected(android.app.ActionBar.Tab tab,
				android.app.FragmentTransaction ft) {
			// TODO Auto-generated method stub
			
		}
		
	    }
