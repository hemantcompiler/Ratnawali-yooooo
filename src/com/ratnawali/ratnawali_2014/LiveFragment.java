package com.ratnawali.ratnawali_2014;


import java.util.Observable;
import java.util.Observer;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
 
public class LiveFragment extends Fragment implements Observer {
	
	private static final String TAG = "MyActivity";
	private static final String TAG1 = "ec";
 
    @SuppressLint({ "SetJavaScriptEnabled", "NewApi", "JavascriptInterface" }) @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
    	
    	View rootView = inflater.inflate(R.layout.webview, container, false);
    	final WebView wv = (WebView)rootView.findViewById(R.id.webview);
    	
        //wv.loadUrl("file:///android_asset/sponsers.html");
    	
    	Button newButton = (Button)rootView.findViewById(R.id.new_button);
    	
    	newButton.setOnClickListener(new View.OnClickListener() {
    		   public void onClick(View v) {
    			   wv.loadUrl("http://ratnawali.netii.net/live.php");
    		   }});
    //code commented out to insert new WebViewClient
    	
    /*	wv.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedError(WebView view, int errorCode,
                    String description, String failingUrl) {
            	wv.loadUrl("file:///android_asset/live.html");
               
            }         
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });*/
    	
    	//newly added code -->
    	// Also a new class is created namely HtmlJSInterface 
    	wv.setWebViewClient(new MyWebViewClient());
    	HtmlJSInterface htmlJSInterface = new HtmlJSInterface();
    	wv.addJavascriptInterface(htmlJSInterface, "HTMLOUT");
    	htmlJSInterface.addObserver(this);
    	//<--
    	
		wv.getSettings().setJavaScriptEnabled(true);
		wv.getSettings().setLoadWithOverviewMode(true);
   	 	wv.getSettings().setUseWideViewPort(true);
   	 	wv.getSettings().setBuiltInZoomControls(true);
   	 	wv.getSettings().setDisplayZoomControls(false);
   	 	
		wv.loadUrl("http://ratnawali.netii.net/live.php");
    	

        return rootView; 
    }

	@Override
	public void update(Observable observable, Object observation) {
		// Got full page source.
		 if (observable instanceof HtmlJSInterface) {
			 SharedPreferences pref = getActivity().getPreferences(0);
			 SharedPreferences.Editor edt = pref.edit();
			 edt.putString("code", (String) observation);
			 edt.commit();
			 
			 
		       Log.v(TAG, (String) observation);
		       
		        onHtmlChanged();
		    }
		
	}
	
	private void onHtmlChanged() {
	    // Do stuff here...
		
	}
	
	private class MyWebViewClient extends WebViewClient {
	    @Override
		public void onReceivedError(WebView view, int errorCode,
				String description, String failingUrl) {
	    	
	    	SharedPreferences pref = getActivity().getPreferences(0);
	    	// if code is not saved in the preferences then the code written will be returned and saved in the html string
	    	String html = pref.getString("code", "<html><head></head><body><h1>No internet connection</h1></body></html>");
	    	
	    	 view.loadDataWithBaseURL(null, html,"text/html", "UTF-8", null);
	    	 view.loadDataWithBaseURL(null,html + "<script>window.onload = function  () {var division = document.createElement('div'); division.id='fixed'; var para = document.createElement('p'); para.id = 'texthere'; division.appendChild(para); document.getElementsByTagName('body')[0].appendChild(division); document.getElementById('texthere').innerHTML = 'Internet not connected'; document.getElementById('fixed').style.position='fixed'; document.getElementById('fixed').style.top = '1em'; document.getElementById('fixed').style.right = '1em'; document.getElementById('fixed').style.background = '#BD42A5'; document.getElementById('fixed').style.color = 'white';}</script>","text/html", "UTF-8", null);
			
		}

		@Override
	    public boolean shouldOverrideUrlLoading(WebView view, String url) {
	        view.loadUrl("http://ratnawali.netii.net/live.php");
	        return true;
	    }

	    @Override
	    public void onPageFinished(WebView view, String url) {
	        // When each page is finished we're going to inject our custom
	        // JavaScript which allows us to
	        // communicate to the JS Interfaces. Responsible for sending full
	        // HTML over to the
	        // HtmlJSInterface...
	      /*  isStarted = false;
	        isLoaded = true;
	        timeoutTimer.cancel();*/
	    	
	        view.loadUrl("javascript:(function() { "
	                + "window.HTMLOUT.setHtml('<html>'+"
	                + "document.getElementsByTagName('html')[0].innerHTML+'</html>');})();");
	        
	        }
	    }
	
	
}