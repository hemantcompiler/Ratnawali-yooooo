package com.ratnawali.ratnawali_2014;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class TaskPagerAdapter extends FragmentPagerAdapter{

	public TaskPagerAdapter(FragmentManager fm) {
		super(fm);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Fragment getItem(int index) {
		// TODO Auto-generated method stub
		switch (index) {
        case 0:
            // Top Rated fragment activity
            return new LiveFragment();
        case 1:
            // Games fragment activity
            return new HistoryFragment();
        case 2:
            // Movies fragment activity
            return new EventsFragment();
        case 3:
            // Movies fragment activity
            return new GalleryFragment();
        case 4:
            // Movies fragment activity
            return new ContactUs();
        
        }
 
        return null;
		
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		// get item count - equal to number of tabs
		return 5;
	}

}
