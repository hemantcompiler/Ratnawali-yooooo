package com.ratnawali.ratnawali_2014;


import java.util.ArrayList;
import java.util.Arrays;

import com.ratnawali.ratnawali_2014.R;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
 
public class EventsFragment extends Fragment {
	private ListView mainListView ;  
	  private ArrayAdapter<String> listAdapter ;   
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
    	View rootView = inflater.inflate(R.layout.list_view, container, false);
    	mainListView = (ListView) rootView.findViewById( R.id.mainListView );
    	
    	String[] planets = new String[] { "Inaugration","Pop song haryanvi", "Haryanvi bhajan", "Haryanvi Skit","Tit bits", "Declamation",  
                "Solo Dance", "Haryanvi Folk","hasya kavi sammelan", "Gazal haryanvi", "Haryanvi Lokgeet","Haryanvi quiz",
                "Group dance(rasiya)","Monoacting","Saang competition","Short film","Antique haryanvi collection exhibition",
                "Sangeet Sandhya","Haryanvi Orchestra","Group song Haryanvi","Raagni","Poetry","Choupal","Folk Costume",
                "One Act Play","Spot painting","Painting Exhibition","Haryanvi Group Dance","Prize Distribution"};    
ArrayList<String> planetList = new ArrayList<String>();  
planetList.addAll( Arrays.asList(planets) );  
listAdapter = new ArrayAdapter<String>(this.getActivity(), R.layout.simplerow, planetList);  
 
mainListView.setAdapter( listAdapter );

mainListView.setOnItemClickListener(new OnItemClickListener() {

  

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		int itm=arg2;
        switch (itm) {
        case 0:
        
        Intent intent = new Intent(getActivity(), Inaugration.class);
        startActivity(intent);
            break;
        case 1:
        	Intent intent1 = new Intent(getActivity(), Popsong.class);
            startActivity(intent1);
                break;
        case 2:
        	Intent intent2 = new Intent(getActivity(), HaryanviBhajan.class);
            startActivity(intent2);
                break;
        case 3:
        	Intent intent3 = new Intent(getActivity(), HaryanviSkit.class);
            startActivity(intent3);
                break;
        case 4:
              Intent intent4 = new Intent(getActivity(), Titbits.class);
              startActivity(intent4);
                        break;
        case 5:
            Intent intent5 = new Intent(getActivity(), Declamation.class);
            startActivity(intent5);
                      break;
        case 6:
            Intent intent6 = new Intent(getActivity(), SoloDance.class);
            startActivity(intent6);
                      break;
        case 7:
            Intent intent7 = new Intent(getActivity(), HaryanviFolk.class);
            startActivity(intent7);
                      break;
        case 8:
            Intent intent8 = new Intent(getActivity(), HasyaKaviSammelan.class);
            startActivity(intent8);
                      break;
        case 9:
            Intent intent9 = new Intent(getActivity(), GazalHaryanvi.class);
            startActivity(intent9);
                      break;
        case 10:
            Intent intent10 = new Intent(getActivity(), LokGeet.class);
            startActivity(intent10);
                      break;
        case 11:
            Intent intent11 = new Intent(getActivity(), HaryanviQuiz.class);
            startActivity(intent11);
                      break;
        case 12:
            Intent intent12 = new Intent(getActivity(), GroupDanceRasiya.class);
            startActivity(intent12);
                      break;
        case 13:
            Intent intent13 = new Intent(getActivity(), Monoacting.class);
            startActivity(intent13);
                      break;
        case 14:
            Intent intent14 = new Intent(getActivity(), SaangCompetition.class);
            startActivity(intent14);
                      break;
        case 15:
            Intent intent15 = new Intent(getActivity(), ShortFilm.class);
            startActivity(intent15);
                      break;
        case 16:
            Intent intent16 = new Intent(getActivity(), CollectionExhibition.class);
            startActivity(intent16);
                      break;
        case 17:
            Intent intent17 = new Intent(getActivity(), SangeetSandhya.class);
            startActivity(intent17);
                      break;
        case 18:
            Intent intent18 = new Intent(getActivity(), HaryanviOrchestra.class);
            startActivity(intent18);
                      break;
        case 19:
            Intent intent19 = new Intent(getActivity(), GroupSongHaryanvi.class);
            startActivity(intent19);
                      break;
        case 20:
            Intent intent20 = new Intent(getActivity(), Raagni.class);
            startActivity(intent20);
                      break;
        case 21:
            Intent intent21 = new Intent(getActivity(), Poetry.class);
            startActivity(intent21);
                      break;
        case 22:
            Intent intent22 = new Intent(getActivity(), Choupal.class);
            startActivity(intent22);
                      break;
        case 23:
            Intent intent23 = new Intent(getActivity(), FolkCostume.class);
            startActivity(intent23);
                      break;
        case 24:
            Intent intent24 = new Intent(getActivity(), OneActPlay.class);
            startActivity(intent24);
                      break;
        case 25:
            Intent intent25 = new Intent(getActivity(), SpotPainting.class);
            startActivity(intent25);
                      break;
        case 26:
            Intent intent26 = new Intent(getActivity(), PaintingExhibition.class);
            startActivity(intent26);
                      break;
        case 27:
            Intent intent27 = new Intent(getActivity(), HaryanviGroupDance.class);
            startActivity(intent27);
                      break;
        case 28:
            Intent intent28 = new Intent(getActivity(), PrizeDistribution.class);
            startActivity(intent28);
                      break;
        
                   
                  //..............................
        }
		
	}});
    	
    	
    	 
        return rootView;
    }
 
}